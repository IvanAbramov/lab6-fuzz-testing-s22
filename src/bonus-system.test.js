import {calculateBonuses} from "./bonus-system.js";
 
const assert = require("assert");
 

describe('Calculator tests', () => {
 
    let app;
 
    const acceptablePrecision = 5;
 
    console.log("Tests started");
 

 

 
    test('Standard  10000',  (done) => {
 

 
        expect(calculateBonuses('Standard', 10000)).toBeCloseTo(0.05 * 1.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
        test('Standard 50000',  (done) => {
 

 
        expect(calculateBonuses('Standard', 50000)).toBeCloseTo(0.05 * 2, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Standard  100000',  (done) => {
 

 
        expect(calculateBonuses('Standard', 100000)).toBeCloseTo(0.05 * 2.5, acceptablePrecision);
 
        done();
 
    });
 

 

 
    test('Premium  10000',  (done) => {
 

 
        expect(calculateBonuses('Premium', 10000)).toBeCloseTo(0.1 * 1.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
        test('Premium 50000',  (done) => {
 

 
        expect(calculateBonuses('Premium', 50000)).toBeCloseTo(0.1 * 2, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Premium  100000',  (done) => {
 

 
        expect(calculateBonuses('Premium', 100000)).toBeCloseTo(0.1 * 2.5, acceptablePrecision);
 
        done();
 
    });
 

 
    test('Diamond  10000',  (done) => {
 

 
        expect(calculateBonuses('Diamond', 10000)).toBeCloseTo(0.2 * 1.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
        test('Diamond 50000',  (done) => {
 

 
        expect(calculateBonuses('Diamond', 50000)).toBeCloseTo(0.2 * 2, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Diamond  100000',  (done) => {
 

 
        expect(calculateBonuses('Diamond', 100000)).toBeCloseTo(0.2 * 2.5, acceptablePrecision);
 
        done();
 
    });
 

 
    test('Standard < 10000',  (done) => {
 

 
        expect(calculateBonuses('Standard', 1)).toBeCloseTo(0.05 * 1, acceptablePrecision);
 
        done();
 
    });
 
    
 
    
 
    test('Standard (10000, 50000)',  (done) => {
 

 
        expect(calculateBonuses('Standard', 10001)).toBeCloseTo(0.05 * 1.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
    
 
    test('Standard (50000, 100000)',  (done) => {
 

 
        expect(calculateBonuses('Standard', 50001)).toBeCloseTo(0.05 * 2, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Standard > 100000',  (done) => {
 

 
        expect(calculateBonuses('Standard', 100001)).toBeCloseTo(0.05 * 2.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Premium < 10000',  (done) => {
 

 
        expect(calculateBonuses('Premium', 1)).toBeCloseTo(0.1 * 1, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Premium (10000, 50000)',  (done) => {
 

 
        expect(calculateBonuses('Premium', 10001)).toBeCloseTo(0.1 * 1.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Premium (50000, 100000)',  (done) => {
 

 
        expect(calculateBonuses('Premium', 50001)).toBeCloseTo(0.1 * 2, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Premium > 100000',  (done) => {
 

 
        expect(calculateBonuses('Premium', 100001)).toBeCloseTo(0.1 * 2.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Diamond < 10000',  (done) => {
 

 
        expect(calculateBonuses('Diamond', 1)).toBeCloseTo(0.2 * 1, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Diamond (10000, 50000)',  (done) => {
 

 
        expect(calculateBonuses('Diamond', 10001)).toBeCloseTo(0.2 * 1.5, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Diamond (50000, 100000)',  (done) => {
 

 
        expect(calculateBonuses('Diamond', 50001)).toBeCloseTo(0.2 * 2, acceptablePrecision);
 
        done();
 
    });
 
    
 
    test('Diamond > 100000',  (done) => {
 

 
        expect(calculateBonuses('Diamond', 100001)).toBeCloseTo(0.2 * 2.5, acceptablePrecision);
 
        done();
 
    });
 

 
    test('Non existing',  (done) => {
 

 
        expect(calculateBonuses('nonsense', 1)).toBeCloseTo(0, acceptablePrecision);
 
        done();
 
    });

 
    console.log('Tests Finished');
 

 
});
